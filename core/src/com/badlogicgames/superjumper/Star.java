package com.badlogicgames.superjumper;

/**
 * Created by Martín on 14/04/2017.
 */

public class Star extends GameObject{
    public static float STAR_WIDTH = 0.3f;
    public static float STAR_HEIGHT = 0.3f;
    float stateTime;
    public Star(float x,float y){super(x,y,STAR_WIDTH,STAR_HEIGHT);}
    public void update (float deltaTime) {
        stateTime += deltaTime;
    }
}

